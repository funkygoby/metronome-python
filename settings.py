# -*- coding: utf-8 -*-

import configparser
import os
from PyQt5 import QtWidgets

from consts import *

class ContainerSettings():
    def __init__(self, ticChoice='generated', ticFreq=660, ticLength=50, tocChoice='generated', tocFreq=440, tocLength=50):
        self.ticChoice = ticChoice
        self.ticFreq = ticFreq
        self.ticLength = ticLength
        self.tocChoice = tocChoice
        self.tocFreq = tocFreq
        self.tocLength = tocLength

    def load(self):
        config = configparser.ConfigParser()
        config.read(Paths.FILE_SETTINGS)
        self.ticChoice = config.get('settings', 'ticChoice', fallback='generated')
        self.ticFreq = config.getint('settings', 'ticFreq', fallback=660)
        self.ticLength = config.getint('settings', 'ticLength', fallback=50)
        self.tocChoice = config.get('settings', 'tocChoice', fallback='generated')
        self.tocFreq = config.getint('settings', 'tocFreq', fallback=440)
        self.tocLength = config.getint('settings', 'tocLength', fallback=50)

        return self

    def write(self):
        if not os.path.exists(Paths.FOLDER_CONFIG):
            os.makedirs(Paths.FOLDER_CONFIG, exist_ok=True)

        with open(Paths.FILE_SETTINGS, 'w') as cfgfile:
            config = configparser.ConfigParser()
            config.add_section('settings')
            config.set('settings', 'ticChoice', str(self.ticChoice))
            config.set('settings', 'ticFreq', str(self.ticFreq))
            config.set('settings', 'ticLength', str(self.ticLength))
            config.set('settings', 'tocChoice', str(self.tocChoice))
            config.set('settings', 'tocFreq', str(self.tocFreq))
            config.set('settings', 'tocLength', str(self.tocLength))
            config.write(cfgfile)

class LayoutTic(QtWidgets.QVBoxLayout):
    def __init__(self, name, parent=None):
        super().__init__(parent)

        # Layout doesn't have the setVisible capability. We need to pack it in a widget first
        self.wDefault = QtWidgets.QWidget()
        layout = QtWidgets.QHBoxLayout()
        self.checkbox = QtWidgets.QCheckBox()
        label = QtWidgets.QLabel('Use general settings sounds for '+name)
        layout.addWidget(self.checkbox)
        layout.addWidget(label)
        self.wDefault.setLayout(layout)
        
        # This is UGLY
        self.wChoice = QtWidgets.QWidget() # The main layout for choosing sounds
        layoutChoice = QtWidgets.QVBoxLayout()
        layout = QtWidgets.QHBoxLayout()
        label = QtWidgets.QLabel(name)
        self.choice = QtWidgets.QComboBox()
        files = os.listdir(Paths.FOLDER_SOUNDS)
        self.choice.addItems(['generated', 'muted'])
        self.choice.addItems(files)
        layout.addWidget(label)
        layout.addWidget(self.choice)
        layoutChoice.addLayout(layout)
        self.wChoice.setLayout(layoutChoice)

        self.wGen = QtWidgets.QWidget() # Optional layout for Gen sound
        layoutGen = QtWidgets.QHBoxLayout()
        labelFreq = QtWidgets.QLabel('Freq')
        self.freq = QtWidgets.QLineEdit()
        labelLength = QtWidgets.QLabel('Length')
        self.length = QtWidgets.QLineEdit()
        layoutGen.addWidget(labelFreq)
        layoutGen.addWidget(self.freq)
        layoutGen.addWidget(labelLength)
        layoutGen.addWidget(self.length)
        self.wGen.setLayout(layoutGen)
        layoutChoice.addWidget(self.wGen)
        # We need to declare what is being manipulated in the callbacks before setting the callbacks
        self.checkbox.stateChanged.connect(self.onCheck)
        self.choice.currentIndexChanged.connect(self.onCombo)

        self.addWidget(self.wDefault)
        self.addWidget(self.wChoice)

        self.wDefault.setVisible(False) # Hide the header for general purposes
        self.onCombo() # Call this to decide wether or not to show the Gen

    def onCheck(self):
        if self.checkbox.isChecked():
            self.wChoice.setVisible(False)
        else:
            self.wChoice.setVisible(True)

    def onCombo(self):
        index = str(self.choice.currentText())
        if index == 'generated':
            self.wGen.setVisible(True)
        else:
            self.wGen.setVisible(False)

class Settings(QtWidgets.QDialog):
    def __init__(self, ticChoice=None, tocChoice=None, parent=None):
        super().__init__(parent)
        self.setModal(True)

        self.container = ContainerSettings().load() # By starting with exec_ and closing with close, we can retrieve members of the instance.
        if ticChoice is not None:
            self.container.ticChoice = ticChoice
        if tocChoice is not None:
            self.container.tocChoice = tocChoice

        self.setWindowTitle('Settings')
        layout = QtWidgets.QVBoxLayout()
        
        self.layoutTic = LayoutTic('tic')
        self.layoutToc = LayoutTic('toc')
        layoutButtons = QtWidgets.QHBoxLayout()
        ok = QtWidgets.QPushButton('OK')
        ok.clicked.connect(self.onOk)
        cancel = QtWidgets.QPushButton('Cancel')
        cancel.clicked.connect(self.onCancel)
        layoutButtons.addWidget(ok)
        layoutButtons.addWidget(cancel)

        layout.addLayout(self.layoutTic)
        layout.addLayout(self.layoutToc)
        layout.addLayout(layoutButtons)
        self.setLayout(layout)

        self.containerToUi()

    def containerToUi(self):
        if self.container.ticChoice == '': # Use default sound for settingsMini
            self.layoutTic.checkbox.setChecked(True)
        else:
            index = self.layoutTic.choice.findText(self.container.ticChoice)
            self.layoutTic.choice.setCurrentIndex(index)
        self.layoutTic.freq.setText(str(self.container.ticFreq))
        self.layoutTic.length.setText(str(self.container.ticLength))
        if self.container.tocChoice =='':
            self.layoutToc.checkbox.setChecked(True)
        else:
            index = self.layoutToc.choice.findText(self.container.tocChoice)
            self.layoutToc.choice.setCurrentIndex(index)
        self.layoutToc.freq.setText(str(self.container.tocFreq))
        self.layoutToc.length.setText(str(self.container.tocLength))

    def uiToContainer(self):
        try:
            ticFreq = int(self.layoutTic.freq.text())
            ticLength = int(self.layoutTic.length.text())
            tocFreq = int(self.layoutToc.freq.text())
            tocLength = int(self.layoutToc.length.text())
        except:
            print('Generating Values must be int')
            return False

        if self.layoutTic.checkbox.isChecked():
            self.container.ticChoice = ''
        else:
            self.container.ticChoice = self.layoutTic.choice.currentText()
        self.container.ticFreq = ticFreq
        self.container.ticLength = ticLength
        if self.layoutToc.checkbox.isChecked():
            self.container.tocChoice = ''
        else:
            self.container.tocChoice = self.layoutToc.choice.currentText()
        self.container.tocFreq = tocFreq
        self.container.tocLength = tocLength

        return True

    def onCancel(self):
        self.close()

    def onOk(self):
        if not self.uiToContainer():
            return # Retry
        self.container.write()
        self.close()

class SettingsMini(Settings):
    # Used from the mode configuration dialog when we want to change specific sounds of bases/rhythms
    def __init__(self, ticChoice, tocChoice, parent=None):
        super().__init__(ticChoice, tocChoice, parent)
        self.layoutTic.wDefault.setVisible(True)
        self.layoutToc.wDefault.setVisible(True)

    def onOk(self): # Do not write the settings config file
        if not self.uiToContainer():
            return # Retry
        self.close()