# -*- coding: utf-8 -*-

import os
import pickle
from PyQt5 import QtWidgets

from consts import *

class ContainerRandomMute():
    def __init__(self, freqMean=4, freqDev=2, lengthMean=4, lengthDev=2):
        self.freqMean = freqMean
        self.freqDev = freqDev
        self.lengthMean = lengthMean
        self.lengthDev = lengthDev

    def load(self):
        # It is safer to use try/except instead of os.path.isfile() because the file may be moved/deleted
        try:
            with open(Paths.FILE_RANDOM, 'rb') as randfile:
                self = pickle.load(randfile)
        except FileNotFoundError:
            pass

        return self

    def write(self):
        if not os.path.exists(Paths.FOLDER_CONFIG):
            os.makedirs(Paths.FOLDER_CONFIG, exist_ok=True)

        with open(Paths.FILE_RANDOM, 'wb') as randfile:
            pickle.dump(self, randfile)

class RandomMute(QtWidgets.QDialog):
    def __init__(self, container=None, parent=None):
        super().__init__(parent)
        self.setModal(True)

        if container == None:
            self.container = ContainerRandomMute().load()
        else:
            self.container = container

        self.setWindowTitle(Consts.COMBOBOX[1])
        layout = QtWidgets.QVBoxLayout()
        
        labelFreq = QtWidgets.QLabel('Mute Frequency (in sec)')
        layoutFreq = QtWidgets.QHBoxLayout()
        labelFreqMean = QtWidgets.QLabel('Every')
        self.wFreqMean = QtWidgets.QLineEdit()
        labelFreqDev = QtWidgets.QLabel('+/-')
        self.wFreqDev = QtWidgets.QLineEdit()
        layoutFreq.addWidget(labelFreqMean)
        layoutFreq.addWidget(self.wFreqMean)
        layoutFreq.addWidget(labelFreqDev)
        layoutFreq.addWidget(self.wFreqDev)

        labelLength = QtWidgets.QLabel('Mute Duration (in sec)')
        layoutLength =  QtWidgets.QHBoxLayout()
        labelLengthMean = QtWidgets.QLabel('Duration mean')
        self.wLengthMean = QtWidgets.QLineEdit()
        labelLengthDev = QtWidgets.QLabel('+/-')
        self.wLengthDev = QtWidgets.QLineEdit()
        layoutLength.addWidget(labelLengthMean)
        layoutLength.addWidget(self.wLengthMean)
        layoutLength.addWidget(labelLengthDev)
        layoutLength.addWidget(self.wLengthDev)

        layoutButtons = QtWidgets.QGridLayout()
        buttonOk = QtWidgets.QPushButton('Ok')
        buttonOk.clicked.connect(self.onOk)
        buttonCancel = QtWidgets.QPushButton('Cancel')
        buttonCancel.clicked.connect(self.onCancel)
        buttonSave = QtWidgets.QPushButton('Save')
        buttonSave.clicked.connect(self.onSave)
        buttonLoad = QtWidgets.QPushButton('Load')
        buttonLoad.clicked.connect(self.onLoad)
        layoutButtons.addWidget(buttonOk, 0, 0)
        layoutButtons.addWidget(buttonCancel, 0, 1)
        layoutButtons.addWidget(buttonSave, 1, 0)
        layoutButtons.addWidget(buttonLoad, 1, 1)

        layout.addWidget(labelFreq)
        layout.addLayout(layoutFreq)
        layout.addWidget(labelLength)
        layout.addLayout(layoutLength)
        layout.addLayout(layoutButtons)
        self.setLayout(layout)

        self.containerToUi()

    def containerToUi(self):
        self.wFreqMean.setText(str(self.container.freqMean))
        self.wFreqDev.setText(str(self.container.freqDev))
        self.wLengthMean.setText(str(self.container.lengthMean))
        self.wLengthDev.setText(str(self.container.lengthDev))

    def uiToContainer(self):
        try:
            freqMean = float(self.wFreqMean.text())
            freqDev = float(self.wFreqDev.text())
            lengthMean = float(self.wLengthMean.text())
            lengthDev = float(self.wLengthDev.text())
        except:
            print('Entries must be float')
            return False
        if freqMean < 0 or freqDev < 0 or lengthMean < 0 or lengthDev < 0:
            print('Entries must be >= 0')
            return False

        self.container.freqMean = freqMean
        self.container.freqDev = freqDev
        self.container.lengthMean = lengthMean
        self.container.lengthDev = lengthDev

        return True

    def onCancel(self):
        self.close()

    def onOk(self):
        if not self.uiToContainer():
            return # Retry
        self.close()

    def onSave(self):
        if not self.uiToContainer():
            return
        self.container.write()

    def onLoad(self):
        self.container = ContainerRandomMute().load()
        self.containerToUi()

