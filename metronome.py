#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import configparser
import os # makedir
import sys # argv
import time #tapin
from threading import Thread
from PyQt5 import QtWidgets


from about import About
from clickers import Clicker
from clickers import ClickerRandomMute
from clickers import ClickerPolyrhythms
from clickers import ClickerStructure
from consts import *
from randommute import ContainerRandomMute
from randommute import RandomMute
from polyrhythms import ContainerPolyrhythms
from polyrhythms import Polyrhythms
from structures import ContainerStructures
from structures import Structures
from settings import ContainerSettings
from settings import Settings

class Metronome(QtWidgets.QMainWindow): # QWidget isn't enough. We need menubar
    def __init__(self):
        super().__init__()
        self.resize(300, 200)

        self.bpm = []
        self.bar = []
        self.lastTapIn = time.time()
        self.mode = Consts.MODE_NORMAL
        self.isRunning = False
        self.settings = None
        self.polyrhythms = None
        self.randomMute = None
        self.structures = None

        config = configparser.ConfigParser()
        config.read(Paths.FILE_INST) # Handles the case of not existing file
        self.mode = config.getint('main', 'mode', fallback=Consts.MODE_NORMAL)
        for i in range(2):
            self.bpm.append(config.getfloat('main', 'bpm'+str(i), fallback=120.))
            self.bar.append(config.getfloat('main', 'bar'+str(i), fallback=4.))
        for i in range(2,4):
            self.bpm.append(config.getfloat('main', 'bpm'+str(i), fallback=100.))
            self.bar.append(config.getfloat('main', 'bar'+str(i), fallback=0.))
        self.settings = ContainerSettings().load()
        self.randomMute = ContainerRandomMute().load()
        self.polyrhythms = ContainerPolyrhythms().load()
        self.structures = ContainerStructures().load()

        self.modeChoices = list(Consts.COMBOBOX[0:Consts.MODES_TOTAL-1])
        self.modeChoices += [Consts.COMBOBOX[Consts.MODES_TOTAL-1]+str(i+1) for i in range(len(self.structures.structures))]

        self.buildGui()

    def buildGui(self):
        menubar = self.menuBar()
        menuConfigure = menubar.addMenu('&Configure')
        menuHelp = menubar.addMenu('&Help')
        
        actionRandomMute = QtWidgets.QAction('&RandomMute', self)
        actionRandomMute.setStatusTip('Open Random Mute Menu')
        actionRandomMute.triggered.connect(self.onRandomMute)
        actionPolyrhythms = QtWidgets.QAction('&Polyrhythms', self)
        actionPolyrhythms.setStatusTip('Open Polyrhythms Menu')
        actionPolyrhythms.triggered.connect(self.onPolyrhythms)
        actionStructures = QtWidgets.QAction('S&tructures', self)
        actionStructures.setStatusTip('Open Structures Menu')
        actionStructures.triggered.connect(self.onStructures)
        actionSettings = QtWidgets.QAction('&Settings', self)
        actionSettings.setStatusTip('Global Settings')
        actionSettings.triggered.connect(self.onSettings)
        actionAbout = QtWidgets.QAction('&About', self)
        actionAbout.setStatusTip('About the metronome')
        actionAbout.triggered.connect(lambda : About(self).exec_())

        menuConfigure.addAction(actionRandomMute)
        menuConfigure.addAction(actionPolyrhythms)
        menuConfigure.addAction(actionStructures)
        menuConfigure.addAction(actionSettings)
        menuHelp.addAction(actionAbout)

        # Main Layout
        layout = QtWidgets.QGridLayout()
        self.wBpm = QtWidgets.QLineEdit(str(self.bpm[min(Consts.MODES_TOTAL-1, self.mode)]))
        self.wBar = QtWidgets.QLineEdit(str(self.bar[min(Consts.MODES_TOTAL-1, self.mode)]))
        self.wTapIn = QtWidgets.QPushButton('Tap In')
        self.wTapIn.clicked.connect(self.onTapIn)
        self.wModes = QtWidgets.QComboBox()
        self.wModes.addItems(self.modeChoices)
        self.wModes.setCurrentIndex(self.mode)
        self.wModes.currentIndexChanged.connect(self.onModes)
        self.wStartStop = QtWidgets.QPushButton('Start')
        self.wStartStop.setFixedSize(300, 100)
        self.wStartStop.clicked.connect(self.onStartStop)
        layout.addWidget(self.wBpm, 0, 0)
        layout.addWidget(self.wBar, 0, 1)
        layout.addWidget(self.wTapIn, 1, 0)
        layout.addWidget(self.wModes, 1, 1)
        layout.addWidget(self.wStartStop, 2, 0, 1, 2) # Spans 2 columns

        # Set the main layout
        centralWidget = QtWidgets.QWidget()
        centralWidget.setLayout(layout)
        self.setCentralWidget(centralWidget)

        self.show()

    def onModes (self): # We just change the textEdit
        index = self.wModes.currentIndex()
        self.wBpm.setText(str(self.bpm[min(Consts.MODES_TOTAL-1, index)]))
        self.wBar.setText(str(self.bar[min(Consts.MODES_TOTAL-1, index)]))
        if index > Consts.MODE_RANDOM:
            print('BPM is now global speed (%) and Bar is repeat times (0 for inf)')

    def onRandomMute(self):
        randomMute = RandomMute(self.randomMute)
        randomMute.exec_()
        if randomMute.container == None:
            print('RandomMute canceled')
            return

        self.randomMute = randomMute.container
        self.wModes.setCurrentIndex(self.mode)

    def onPolyrhythms(self):
        polyrhythms = Polyrhythms(self.polyrhythms)
        polyrhythms.exec_()
        if polyrhythms.container == None:
            print('Polyrhythms canceled')
            return

        self.polyrhythms = polyrhythms.container
        self.wModes.setCurrentIndex(self.mode)

    def onStructures(self):
        structures = Structures(self.structures)
        structures.exec_()
        if structures.container == None:
            print('Structures canceled')
            return

        self.structures = structures.container
        # Update the combobox
        self.modeChoices = list(Consts.COMBOBOX[0:Consts.MODES_TOTAL-1])
        self.modeChoices += [Consts.COMBOBOX[Consts.MODES_TOTAL-1]+str(i+1) for i in range(len(self.structures.structures))]
        self.wModes.clear()
        self.wModes.addItems(self.modeChoices)

        self.wModes.setCurrentIndex(self.mode)
            
    def onSettings(self):
        settings = Settings()
        settings.exec_()
        if settings.container == None:
            print('Settings canceled')
            return

        self.settings = settings.container

    def onTapIn(self):
        now = time.time()
        bpm = 60./(now - self.lastTapIn)
        self.lastTapIn = now
        self.wBpm.setText(str(bpm))

    def onStartStop(self):
        if not self.isRunning: # Go!
            try:
                bpm = float(self.wBpm.text())
                bar = float(self.wBar.text())
            except:
                print('BPM or Bar is not a valid float value')
                return

            if bpm <= 0 or bar < 0:
                print('BPM and Bar must be >0')
                return

            self.mode = self.wModes.currentIndex()
            self.bpm[min(Consts.MODES_TOTAL-1, self.mode)] = bpm
            self.bar[min(Consts.MODES_TOTAL-1, self.mode)] = bar

            '''
            There may be an overflow if the bpm is to low (the signal is too long). We may separate the signal generation from the constructor in order to make proper checks but I chose to but everything in this try/catch
            '''
            if self.mode == Consts.MODE_NORMAL:
                try:
                    self.clic = Clicker(self.settings, bpm, bar)
                except MemoryError:
                    print('BPM too low')
                    return
                self.clic.setDaemon(True) # It will be killed automatically if the main window is closed
            elif self.mode == Consts.MODE_POLYRHYTHMS:
                try:
                    self.clic = ClickerPolyrhythms(self.settings, self.polyrhythms, bpm)
                except MemoryError:
                    print('BPM too low')
                    return
            elif self.mode == Consts.MODE_RANDOM:
                try:
                    self.clic = ClickerRandomMute(self.settings, self.randomMute, bpm, bar)
                except MemoryError:
                    print('BPM too low')
                    return
                self.clic.setDaemon(True)
            else:
                try:
                    self.clic = ClickerStructure(self.settings, self.structures, self.mode - (Consts.MODES_TOTAL-1), bpm, bar)
                except MemoryError:
                    print('BPM too low')
                    return
                self.clic.setDaemon(True)
            self.clic.start()

            self.wStartStop.setText('Stop')
            self.isRunning = True
        else:
            self.clic.stop()

            self.wStartStop.setText('Start')
            self.isRunning = False

    def closeEvent(self, event): # Override the close event
        if not os.path.exists(Paths.FOLDER_CONFIG):
            os.makedirs(Paths.FOLDER_CONFIG, exist_ok=True)

        with open(Paths.FILE_INST, 'w') as cfgfile:
            config = configparser.ConfigParser()
            config.add_section('main')
            config.set('main', 'mode', str(self.mode))
            for i in range(4):
                config.set('main', 'bpm'+str(i), str(self.bpm[i]))
                config.set('main', 'bar'+str(i), str(self.bar[i]))
            config.write(cfgfile)

        event.accept()

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    w = Metronome()
    sys.exit(app.exec())
