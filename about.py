# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import QMessageBox

class About(QMessageBox):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setText('Metronome')
        self.setInformativeText('A port from the android version')
        self.setDetailedText('Get the source code:\nhttps://gitlab.com/funkygoby/metronome-python\n\n The Android version:\nhttps://f-droid.org/en/packages/com.arnaud.metronome\nhttps://gitlab.com/funkygoby/metronome-python')

