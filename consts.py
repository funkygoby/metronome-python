# Define every hardcoded paths and variables

class Paths():
    FOLDER_CONFIG = 'config/'
    FOLDER_SOUNDS = 'sounds/'
    FILE_INST = FOLDER_CONFIG + 'instance.conf'
    FILE_SETTINGS = FOLDER_CONFIG + 'settings.conf'
    FILE_RANDOM = FOLDER_CONFIG + 'randommute.conf'
    FILE_POLYRHYTHMS = FOLDER_CONFIG + 'polyrhythms.pickle'
    FILE_STRUCTURES = FOLDER_CONFIG + 'structures.pickle'

class Consts():
    COMBOBOX = ('Normal', 'Random Mute', 'Polyrhythms', 'Structure')
    MODE_NORMAL = 0
    MODE_RANDOM = 1
    MODE_POLYRHYTHMS = 2
    MODES_TOTAL = 4 # 3+1 with the structures, this is a boundary, we don't want to go too far with bpm/bar[]
    SAMPLERATE = 44100
    
