# -*- coding: utf-8 -*-

import os
import pickle
from PyQt5 import QtGui
from PyQt5 import QtWidgets

from consts import *
from settings import SettingsMini

class Base():
    def __init__(self, bpm=120., bar=4., ticChoice='', tocChoice=''):
        self.bpm = bpm
        self.bar = bar
        self.ticChoice = ticChoice
        self.tocChoice = tocChoice

        # Keep the sounds here
        self.tic = None
        self.toc = None
        self.partialTic = None
        self.partialToc = None
        self.isSet = False

class Pattern():
    class Base():
        def __init__(self, index, times):
            self.index = index
            self.times = times

    def __init__(self):
        self.bases = []
        self.isSet= False

class Structure():
    class Pattern():
        def __init__(self, index, times):
            self.index = index
            self.times = times

    def __init__(self):
        self.patterns = []

class ContainerStructures():
    def __init__(self):
        self.structures = []
        self.patterns = []
        self.bases = []

    def load(self):
        try:
            with open(Paths.FILE_STRUCTURES, 'rb') as structfile:
                self = pickle.load(structfile)
        except FileNotFoundError:
            pass

        return self

    def write(self):
        if not os.path.exists(Paths.FOLDER_CONFIG):
            os.makedirs(Paths.FOLDER_CONFIG, exist_ok=True)

        with open(Paths.FILE_STRUCTURES, 'wb') as structfile:
            pickle.dump(self, structfile)

class LayoutBase(QtWidgets.QHBoxLayout):
    def __init__(self, main, index, bpm, bar, ticChoice, tocChoice, parent=None):
        super().__init__(parent)
        self.index = index # Ease the renaming
        self.main = main
        self.label = QtWidgets.QLabel('base'+str(index+1))
        self.wBpm = QtWidgets.QLineEdit(str(bpm))
        self.wBar = QtWidgets.QLineEdit(str(bar))
        iconSound = QtGui.QIcon('icons/speaker.png')
        buttonSounds = QtWidgets.QPushButton()
        buttonSounds.setIcon(iconSound)
        buttonSounds.clicked.connect(self.onSounds)
        iconDelete = QtGui.QIcon('icons/trash.png')
        buttonDelete = QtWidgets.QPushButton()
        buttonDelete.setIcon(iconDelete)
        buttonDelete.clicked.connect(self.onDelete)

        self.addWidget(self.label)
        self.addWidget(self.wBpm)
        self.addWidget(self.wBar)
        self.addWidget(buttonSounds)
        self.addWidget(buttonDelete)

        self.ticChoice = ticChoice
        self.tocChoice = tocChoice
    
    def onDelete(self):
        # Rename the following bases
        for i in range(self.index, self.main.topLayoutBases.layoutBases.count() - 1):
            self.main.topLayoutBases.layoutBases.itemAt(i+1).label.setText('base'+str(i+1))
            self.main.topLayoutBases.layoutBases.itemAt(i+1).index = i
        # Update comboboxes, remove the last elmt. Leave the user to correct his patterns
        for p in self.main.topLayoutPatterns.layoutPatterns.children():
            for b in p.layoutBases.children():
                count = b.wcombo.count()
                b.wcombo.removeItem(count - 1)
        # Delete the widget
        for i in range(self.count()):
            w = self.itemAt(i)
            w.widget().deleteLater()
        self.deleteLater()
        self.setParent(None) # Very important!! This removes self from its parent's children. This is important if we are looping somewhere else later but before the events loop has been able to cycle. Without this, we would still see self as children() but the object might be in an undefined state (errors, unaccessible objects)

    def onSounds(self):
        settingsMini = SettingsMini(ticChoice=self.ticChoice, tocChoice=self.tocChoice)
        settingsMini.exec_()
        if settingsMini.container == None:
            print('SettingsMini canceled')
            return
        self.ticChoice = settingsMini.container.ticChoice
        self.tocChoice = settingsMini.container.tocChoice

class LayoutPattern(QtWidgets.QVBoxLayout):
    class LayoutBase(QtWidgets.QHBoxLayout):
        def __init__(self, index, times, listBases, parent=None):
            super().__init__(parent)
            self.wcombo = QtWidgets.QComboBox()
            self.wcombo.addItems(listBases)
            self.wcombo.setCurrentIndex(index)
            label = QtWidgets.QLabel(' x ')
            self.times = QtWidgets.QLineEdit(str(times))
            iconDelete = QtGui.QIcon('icons/trash.png')
            buttonDelete = QtWidgets.QPushButton()
            buttonDelete.setIcon(iconDelete)
            buttonDelete.clicked.connect(self.onDelete)

            self.addWidget(self.wcombo)
            self.addWidget(label)
            self.addWidget(self.times)
            self.addWidget(buttonDelete)

        def onDelete(self):
            for i in range(self.count()):
                w = self.itemAt(i)
                w.widget().deleteLater()
            self.deleteLater()

    def __init__(self, main, index, parent=None):
        super().__init__(parent)
        self.index = index
        self.main = main
        self.layoutHeader = QtWidgets.QHBoxLayout()
        self.label = QtWidgets.QLabel('pattern'+str(index+1))
        iconAdd = QtGui.QIcon('icons/add.png')
        buttonAddBase = QtWidgets.QPushButton('Add Base')
        buttonAddBase.setIcon(iconAdd)
        buttonAddBase.clicked.connect(self.onAddBase)
        iconDelete = QtGui.QIcon('icons/trash.png')
        buttonDelete = QtWidgets.QPushButton()
        buttonDelete.setIcon(iconDelete)
        buttonDelete.clicked.connect(self.onDelete)
        self.layoutHeader.addWidget(self.label)
        self.layoutHeader.addWidget(buttonAddBase)
        self.layoutHeader.addWidget(buttonDelete)

        self.layoutBases = QtWidgets.QVBoxLayout()
        
        self.addLayout(self.layoutHeader)
        self.addLayout(self.layoutBases)
    
    def onAddBase(self): # Can't have arguments in callbacks
        self.addBase(-1, 1)

    def addBase(self, index, times):
        listBases = [b.label.text() for b in self.main.topLayoutBases.layoutBases.children()]
        self.layoutBases.addLayout(LayoutPattern.LayoutBase(index, times, listBases))

    def onDelete(self):
        for i in range(self.index, self.main.topLayoutPatterns.layoutPatterns.count() - 1):
            self.main.topLayoutPatterns.layoutPatterns.itemAt(i+1).label.setText('pattern'+str(i+1))
            self.main.topLayoutPatterns.layoutPatterns.itemAt(i+1).index = i
        for s in self.main.topLayoutStructures.layoutStructures.children():
            for p in s.layoutPatterns.children():
                count = p.wcombo.count()
                p.wcombo.removeItem(count - 1)
        # Delete the 2 layout contents
        for i in range(self.layoutHeader.count()):
            w = self.layoutHeader.itemAt(i)
            w.widget().deleteLater()
        for i in range(self.layoutBases.count()):
            w = self.layoutBases.itemAt(i)
            w.onDelete()
        for i in range(self.count()): # Delete the 2 layouts
            w = self.itemAt(i)
            w.deleteLater()
        self.deleteLater()
        self.setParent(None)

class LayoutStructure(QtWidgets.QVBoxLayout):
    class LayoutPattern(QtWidgets.QHBoxLayout):
        def __init__(self, index, times, listPatterns, parent=None):
            super().__init__(parent)
            self.wcombo = QtWidgets.QComboBox()
            self.wcombo.addItems(listPatterns)
            self.wcombo.setCurrentIndex(index)
            label = QtWidgets.QLabel(' x ')
            self.times = QtWidgets.QLineEdit(str(times))
            iconDelete = QtGui.QIcon('icons/trash.png')
            buttonDelete = QtWidgets.QPushButton()
            buttonDelete.setIcon(iconDelete)
            buttonDelete.clicked.connect(self.onDelete)

            self.addWidget(self.wcombo)
            self.addWidget(label)
            self.addWidget(self.times)
            self.addWidget(buttonDelete)

        def onDelete(self):
            for i in range(self.count()):
                w = self.itemAt(i)
                w.widget().deleteLater()
            self.deleteLater()

    def __init__(self, main, index, parent=None):
        super().__init__(parent)
        self.index = index
        self.main = main
        self.layoutHeader = QtWidgets.QHBoxLayout()
        self.label = QtWidgets.QLabel('structure'+str(index+1))
        iconAdd = QtGui.QIcon('icons/add.png')
        buttonAddPattern = QtWidgets.QPushButton('Add Pattern')
        buttonAddPattern.setIcon(iconAdd)
        buttonAddPattern.clicked.connect(self.onAddPattern)
        iconDelete = QtGui.QIcon('icons/trash.png')
        buttonDelete = QtWidgets.QPushButton()
        buttonDelete.setIcon(iconDelete)
        buttonDelete.clicked.connect(self.onDelete)
        self.layoutHeader.addWidget(self.label)
        self.layoutHeader.addWidget(buttonAddPattern)
        self.layoutHeader.addWidget(buttonDelete)

        self.layoutPatterns = QtWidgets.QVBoxLayout()
        
        self.addLayout(self.layoutHeader)
        self.addLayout(self.layoutPatterns)
    
    def onAddPattern(self):
        self.addPattern(-1, 1)

    def addPattern(self, index, times):
        listPatterns = [p.label.text() for p in self.main.topLayoutPatterns.layoutPatterns.children()]
        self.layoutPatterns.addLayout(LayoutStructure.LayoutPattern(index, times, listPatterns))

    def onDelete(self):
        for i in range(self.index, self.main.topLayoutStructures.layoutStructures.count() - 1):
            self.main.topLayoutStructures.layoutStructures.itemAt(i+1).label.setText('structure'+str(i+1))
            self.main.topLayoutStructures.layoutStructures.itemAt(i+1).index = i
        # Delete the header and the layoutPatterns
        for i in range(self.layoutHeader.count()):
            w = self.layoutHeader.itemAt(i)
            w.widget().deleteLater()
        for i in range(self.layoutPatterns.count()):
            w = self.layoutPatterns.itemAt(i)
            w.onDelete()
        for w in self.children():
            w.deleteLater()
        self.deleteLater()
        self.setParent(None)

class TopLayoutBases(QtWidgets.QVBoxLayout):
    def __init__(self, main, parent=None):
        super().__init__(parent)
        self.main = main

        layoutHeader = QtWidgets.QHBoxLayout()
        label = QtWidgets.QLabel('Bases')
        iconAdd = QtGui.QIcon('icons/add.png')
        buttonAddBase = QtWidgets.QPushButton('Create Base')
        buttonAddBase.setIcon(iconAdd)
        buttonAddBase.clicked.connect(self.onAddBase)
        layoutHeader.addWidget(label)
        layoutHeader.addStretch()
        layoutHeader.addWidget(buttonAddBase)

        self.layoutBases = QtWidgets.QVBoxLayout()

        self.addLayout(layoutHeader)
        self.addLayout(self.layoutBases)

    def onAddBase(self):
        self.addBase(120., 4., '', '')
    
    def addBase(self, bpm, bar, ticChoice, tocChoice):
        count = self.layoutBases.count()
        self.layoutBases.addLayout(LayoutBase(self.main, count, bpm, bar, ticChoice, tocChoice))
        # Update comboboxes
        for p in self.main.topLayoutPatterns.layoutPatterns.children():
            for b in p.layoutBases.children():
                b.wcombo.addItem('bases'+str(count+1))

    def clear(self):
        for b in self.layoutBases:
            b.onDelete()

class TopLayoutPatterns(QtWidgets.QVBoxLayout):
    def __init__(self, main, parent=None):
        super().__init__(parent)
        self.main = main

        layoutHeader = QtWidgets.QHBoxLayout()
        label = QtWidgets.QLabel('Patterns')
        iconAdd = QtGui.QIcon('icons/add.png')
        buttonAddPattern = QtWidgets.QPushButton('Create Pattern')
        buttonAddPattern.setIcon(iconAdd)
        buttonAddPattern.clicked.connect(self.onAddPattern)
        layoutHeader.addWidget(label)
        layoutHeader.addStretch()
        layoutHeader.addWidget(buttonAddPattern)

        self.layoutPatterns = QtWidgets.QVBoxLayout()

        self.addLayout(layoutHeader)
        self.addLayout(self.layoutPatterns)

    def onAddPattern(self):
        count = self.layoutPatterns.count()
        self.layoutPatterns.addLayout(LayoutPattern(self.main, count))
        for s in self.main.topLayoutStructures.layoutStructures.children():
            for p in s.layoutPatterns.children():
                p.wcombo.addItem('patterns'+str(count+1))

    def clear(self):
        for p in self.layoutPatterns:
            p.onDelete()

class TopLayoutStructures(QtWidgets.QVBoxLayout):
    def __init__(self, main, parent=None):
        super().__init__(parent)
        self.main = main

        layoutHeader = QtWidgets.QHBoxLayout()
        label = QtWidgets.QLabel('Structures')
        iconAdd = QtGui.QIcon('icons/add.png')
        buttonAddStructure= QtWidgets.QPushButton('Create Structure')
        buttonAddStructure.setIcon(iconAdd)
        buttonAddStructure.clicked.connect(self.onAddStructure)
        layoutHeader.addWidget(label)
        layoutHeader.addStretch()
        layoutHeader.addWidget(buttonAddStructure)

        self.layoutStructures = QtWidgets.QVBoxLayout()

        self.addLayout(layoutHeader)
        self.addLayout(self.layoutStructures)

    def onAddStructure(self):
        count = self.layoutStructures.count()
        self.layoutStructures.addLayout(LayoutStructure(self.main, count))

    def clear(self):
        for s in self.layoutStructures:
            s.onDelete()

class Structures(QtWidgets.QDialog):
    def __init__(self, container=None, parent=None):
        super().__init__(parent)
        self.setModal(True)
        self.resize(300, 120)

        if container == None:
            self.container = ContainerStructures().load()
        else:
            self.container = container

        self.topLayoutBases = TopLayoutBases(self)
        self.topLayoutPatterns = TopLayoutPatterns(self)
        self.topLayoutStructures = TopLayoutStructures(self)

        self.setWindowTitle(Consts.COMBOBOX[3])
        layout = QtWidgets.QVBoxLayout()
        layoutButtons = QtWidgets.QGridLayout()
        buttonOk = QtWidgets.QPushButton('Ok')
        buttonOk.clicked.connect(self.onOk)
        buttonCancel = QtWidgets.QPushButton('Cancel')
        buttonCancel.clicked.connect(self.onCancel)
        buttonSave = QtWidgets.QPushButton('Save')
        buttonSave.clicked.connect(self.onSave)
        buttonLoad = QtWidgets.QPushButton('Load')
        buttonLoad.clicked.connect(self.onLoad)
        layoutButtons.addWidget(buttonOk, 0, 0)
        layoutButtons.addWidget(buttonCancel, 0, 1)
        layoutButtons.addWidget(buttonSave, 1, 0)
        layoutButtons.addWidget(buttonLoad, 1, 1)

        layout.addLayout(self.topLayoutBases)
        layout.addLayout(self.topLayoutPatterns)
        layout.addLayout(self.topLayoutStructures)
        layout.addLayout(layoutButtons)
        self.setLayout(layout)

        self.containerToUi()

    def containerToUi(self):
        for s in self.topLayoutStructures.layoutStructures.children():
            s.onDelete()
        for p in self.topLayoutPatterns.layoutPatterns.children():
            p.onDelete()
        for b in self.topLayoutBases.layoutBases.children():
            b.onDelete()

        for b in self.container.bases:
            self.topLayoutBases.addBase(b.bpm, b.bar, b.ticChoice, b.tocChoice)
        for p in self.container.patterns:
            self.topLayoutPatterns.onAddPattern()
            wp = self.topLayoutPatterns.layoutPatterns.itemAt(self.topLayoutPatterns.layoutPatterns.count()-1)
            for b in p.bases:
                wp.addBase(b.index, b.times)
        for s in self.container.structures:
            self.topLayoutStructures.onAddStructure()
            ws = self.topLayoutStructures.layoutStructures.itemAt(self.topLayoutStructures.layoutStructures.count()-1)
            for p in s.patterns:
                ws.addPattern(p.index, p.times)

    def uiToContainer(self):
        container = ContainerStructures()

        for wb in self.topLayoutBases.layoutBases.children():
            try:
                bpm = float(wb.wBpm.text())
                bar = float(wb.wBar.text())
            except:
                print('bpm and bar must be float')
                return False
            if bpm <= 0 or bar < 0:
                print('bpm > 0 and bar >= 0')
                return False
            ticChoice = wb.ticChoice
            tocChoice = wb.tocChoice
            container.bases.append(Base(bpm, bar, ticChoice, tocChoice))

        for wp in self.topLayoutPatterns.layoutPatterns.children():
            container.patterns.append(Pattern())
            pBases = []
            for wb in wp.layoutBases.children():
                index = wb.wcombo.currentIndex()
                try:
                    times = int(wb.times.text())
                except:
                    print('times must be int')
                    False
                if index < 0 or times < 0:
                    print('index and times must be >= 0')
                    return False
                container.patterns[-1].bases.append(Pattern.Base(index, times))

        for ws in self.topLayoutStructures.layoutStructures.children():
            container.structures.append(Structure())
            sPatterns = []
            for wp in ws.layoutPatterns.children():
                index = wp.wcombo.currentIndex()
                try:
                    times = int(wp.times.text())
                except:
                    print('times must be int')
                    False
                if index < 0 or times < 0:
                    print('index and times must be >= 0')
                    return False
                container.structures[-1].patterns.append(Structure.Pattern(index, times))

        self.container = container

        return True

    def onCancel(self):
        self.close()

    def onOk(self):
        if not self.uiToContainer():
            return # Retry
        self.close()

    def onSave(self):
        if not self.uiToContainer():
            return
        self.container.write()

    def onLoad(self):
        self.container = ContainerStructures().load()
        self.containerToUi()
