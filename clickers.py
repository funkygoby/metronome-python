# -*- coding: utf-8 -*-

import math
import pyaudio
import random
import time
from threading import Thread
# from queue import Queue

from consts import *

def signal (choice, freq, size, bpm):
    if choice == 'generated': # Generate a sin() signal
        return signalGenerate(freq, size, bpm)
    elif choice == 'muted': # Muted, size=0
        return signalGenerate(freq, 0, bpm)
    else: # real sound
        return signalFile(choice, bpm)

def signalGenerate (freq, size, bpm):
    samplerate = Consts.SAMPLERATE
    period = int((60.*samplerate)/bpm)
    w = (2.*math.pi*freq)/samplerate
    sin = []
    #gen = []#XXX Python2
    #gen = bytearray()XXX old python3
    s = bytearray()

    size *= samplerate//1000
    if size >= period:
        size = period//2
    for i in range(size):
        # Normalise int16
        tmp = int(32767*math.sin(i*w))
        # reverse endianness (note the existence of .reverse() for bytearray).
        s.append(tmp & 0xff)
        s.append(tmp >> 8 & 0xff)
    # Fill with 0. 16bits fit in two bytes so we need to double de size
    s.extend(bytes((period-size)*2))
    #Old python2 and python3 fashion
    """
    for i in range(size):
        sin.append(int(32767*math.sin(i*w)))#Normalisation en Signed 16bits. 32767 = 2^(16-1)-1: Max(signed 16bits int). Un bit sert de signe (15bits) et une valeur sert pour coder le 0.
    for i in range(size,period):
        sin.append(0)
    
    i = 0
    while i<len(sin):
    #On peut aussi utiliser struct.pack('<h',sin) pour avoir la représentation en byte mais j'y arrive pas
        #XXX Python2
        '''
        gen.append(chr(sin[i] & 0xff))#Low order byte
        gen.append(chr(sin[i] >> 8 & 0xff))#High order byte
        '''
        #XXX Python3
        gen.append(sin[i] & 0xff)
        gen.append(sin[i] >> 8 & 0xff)
        i += 1
    #gen = ''.join(gen)#XXX Python2
    return bytes(gen)
    """
    return (bytes(s))

def signalFile (filename, bpm):
    samplerate = Consts.SAMPLERATE
    wav = bytearray()
    period = int((60.*samplerate)/bpm)
    s = bytearray()
    with open(Paths.FOLDER_SOUNDS+filename, 'rb') as f:
        chunk = 1024
        data = f.read(chunk)
        while data and len(s) <= period:
            s.extend(data)
            data = f.read(chunk)
    s.extend(bytes(period*2-len(s)))
    
    return bytes(s)

class Clicker(Thread):    
    isRunning = False # This is a class member because it allows to control all instance of clickers remotely (from polyrhythms)
    def __init__(self, settings, bpm, bar):
        super().__init__()
        self.bar = bar

        self.tic = signal(settings.ticChoice, settings.ticFreq, settings.ticLength, bpm)
        self.toc = signal(settings.tocChoice, settings.tocFreq, settings.tocLength, bpm)

        self.pa = pyaudio.PyAudio()
        self.stream = self.pa.open(format=self.pa.get_format_from_width(2), channels=1, rate=Consts.SAMPLERATE, output=True) # stream_callback=self.feed) # 2 is the number of bytes per sample

    def run(self):
        Clicker.isRunning = True
        beat = 0
        bar = self.bar
        # The code flow is important here. Do not change the if's order
        
        if (bar%1 == 0.) and (bar !=0):
            while Clicker.isRunning:
                if beat == 0:
                    self.stream.write(self.tic)
                else:
                    self.stream.write(self.toc)
                beat += 1
                beat %= bar
        elif (bar > 1.):
            partial = self.toc[0:int(len(self.toc)*(bar%1))] # We take a slice of original toc
            self.stream.write(self.tic)
            beat += 1
            while Clicker.isRunning:
                beat %= int(bar)
                if (beat != 0):
                    self.stream.write(self.toc)
                else:
                    self.stream.write(partial)
                    self.stream.write(self.tic)
                beat += 1
        elif (bar == 1.):
            while Clicker.isRunning:
                self.stream.write(self.tic)
        elif (bar == 0.):
            while Clicker.isRunning:
                self.stream.write(self.toc)
        else:
            partial = self.tic[0:int(len(self.tic)*(bar%1))]
            while Clicker.isRunning:
                self.stream.write(partial)
                
    def stop(self):
        Clicker.isRunning = False
        self.join() # The thread merges with the main/UI thread. The main thread now has to wait for this thread to end before going on. This is important to avoid crash, we need the UI thread to wait for stop() to return.
        self.stream.stop_stream()
        self.stream.close()
        self.pa.terminate()

class MuterTask(Thread):
    def __init__(self, randomMute, stream):
        super().__init__()

        self.isMuted = False

        self.freqMean = randomMute.freqMean
        self.freqDev = randomMute.freqDev
        self.lengthMean = randomMute.lengthMean
        self.lengthDev = randomMute.lengthDev
        
        # We intercept the stream.write (as reference) so we can process the signal sent and replace it with 0's
        self.write = stream.write
        stream.write = self.intercept

    def intercept(self, buf):
        # This mute on a per-buf basis. So the precision isn't really following freq and length. For improvement, this should calculate the number of sample to mute
        if self.isMuted:
            buf = bytes([0]*len(buf))
        self.write(buf)

    def run(self):
        while Clicker.isRunning:
            if self.freqDev == 0.:
                freq = self.freqMean
            else:
                freq = random.random()*self.freqDev + self.freqMean
                if freq < 0:
                    freq = 0

            if self.lengthDev == 0.:
                length = self.lengthMean
            else:
                length = random.random()*self.lengthDev + self.lengthMean
                if length < 0:
                    length = 0
            
            # Checks are done here to end the thread promptly if needed
            if not Clicker.isRunning:
                return
            time.sleep(freq) # Wait before muting
            self.isMuted = True
            if not Clicker.isRunning:
                return
            time.sleep(length) # Wait before unmuting
            self.isMuted = False

class ClickerRandomMute(Clicker):
    def __init__(self, settings, randomMute, bpm, bar):
        super().__init__(settings, bpm, bar)
        self.muter = MuterTask(randomMute, self.stream)

    def run(self):
        self.muter.setDaemon(True)
        Clicker.isRunning = True # We need to set this here or else the muter thread will see the False and exit
        self.muter.start()
        super().run()

    def stop(self):
        super().stop()

class ClickerPolyrhythms():
    def __init__(self, settings, polyrhythms, bpm):
        self.clickers = []
        for r in polyrhythms.rhythms:
            self.clickers.append(Clicker(settings, r.bpm/bpm*100, r.bar))
            if r.ticChoice != '':
                self.clickers[-1].tic = signal(r.ticChoice, settings.ticFreq, settings.ticLength, r.bpm/bpm*100)
            if r.tocChoice != '':
                self.clickers[-1].toc = signal(r.tocChoice, settings.tocFreq, settings.tocLength, r.bpm/bpm*100)
            self.clickers[-1].setDaemon(True)

    def start(self):
        Clicker.isRunning = True
        for c in self.clickers:
            c.start()
    
    def stop(self):
        Clicker.isRunning = False
        for c in self.clickers:
            c.stop()

class ClickerStructure(Thread):    
    def __init__(self, settings, structures, i, speed, times):
        Thread.__init__(self)
        self.isRunning = False
        self.bar = times

        self.structure = structures.structures[i]
        self.patterns = structures.patterns
        self.bases = structures.bases

        for sp in self.structure.patterns:
            p = self.patterns[sp.index]
            if not p.isSet:
                for pb in p.bases:
                    b = self.bases[pb.index]
                    if not b.isSet:
                        bpm = b.bpm*speed/100.
                        if b.ticChoice == '': # Same as global
                            b.tic = signal(settings.ticChoice, settings.ticFreq, settings.ticLength, bpm)
                        else:
                            b.tic = signal(b.ticChoice, settings.ticFreq, settings.ticLength, bpm)
                        if b.tocChoice == '': # Same as global
                            b.toc = signal(settings.tocChoice, settings.tocFreq, settings.tocLength, bpm)
                        else:
                            b.toc = signal(b.tocChoice, settings.tocFreq, settings.tocLength, bpm)
                        clip = int(len(b.tic)*(b.bar%1))
                        b.partialTic = b.tic[0:clip]
                        b.partialToc = b.toc[0:clip]
                        b.isSet = True
                p.isSet = True

        self.pa = pyaudio.PyAudio()
        self.stream = self.pa.open(format=self.pa.get_format_from_width(2), channels=1, rate=Consts.SAMPLERATE, output=True)

    def run(self):
        self.isRunning = True
        times = int(self.bar)
        if times == 0:
            times = -1

        while times !=0:
            for sp in self.structure.patterns:
                p = self.patterns[sp.index]
                for i in range(sp.times):
                    for pb in p.bases:
                        b = self.bases[pb.index]
                        tic = b.tic
                        toc = b.toc
                        bar = b.bar
                        partialTic = b.partialTic
                        partialToc = b.partialToc
                            
                        for j in range(pb.times):
                            beat = 0
                            if not self.isRunning: # We need to be able to exit the tree loop also
                                return
                            
                            if bar == 1.:
                                self.stream.write(tic)
                            elif bar == 0.:
                                self.stream.write(toc)
                            elif bar%1 == 0.:
                                while (beat < bar) & self.isRunning:
                                    if beat == 0:
                                        self.stream.write(tic)
                                    else:
                                        self.stream.write(toc)
                                    beat += 1
                            elif bar > 1:
                                self.stream.write(tic)
                                beat += 1
                                while (beat < bar) & self.isRunning:
                                    if beat < int(bar):
                                        self.stream.write(toc)
                                    else:
                                        self.stream.write(partialToc)
                                    beat += 1
                            else:
                                self.stream.write(partialTic)
            times -= 1
        self.isRunning = False
                
    def stop(self):
        self.isRunning = False
        self.join()
        self.stream.stop_stream()
        self.stream.close()
        self.pa.terminate()
        # We set all the bases and patterns to unset. We can not be sure if the user is going to change bpm or sounds or whatever
        for b in self.bases:
            b.isSet = False
        for p in self.patterns:
            p.isSet = False
