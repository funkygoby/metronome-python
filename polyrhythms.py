# -*- coding: utf-8 -*-

import os
import pickle
from PyQt5 import QtGui
from PyQt5 import QtWidgets

from consts import *
from settings import SettingsMini

class Rhythm ():
    def __init__(self, bpm=120., bar=4., ticChoice='', tocChoice=''):
        self.bpm = bpm
        self.bar = bar
        self.ticChoice = ticChoice
        self.tocChoice = tocChoice

class ContainerPolyrhythms():
    def __init__(self):
        self.rhythms = []

    def load(self):
        # It is safer to use try/except instead of os.path.isfile() because the file may be moved/deleted between the check and the open()
        try:
            with open(Paths.FILE_POLYRHYTHMS, 'rb') as polyfile:
                self.rhythms = pickle.load(polyfile)
        except FileNotFoundError:
            pass

        return self

    def write(self):
        if not os.path.exists(Paths.FOLDER_CONFIG):
            os.makedirs(Paths.FOLDER_CONFIG, exist_ok=True)

        with open(Paths.FILE_POLYRHYTHMS, 'wb') as polyfile:
            pickle.dump(self.rhythms, polyfile)

class LayoutRhythm(QtWidgets.QHBoxLayout):
    def __init__(self, bpm=120., bar=4., ticChoice='', tocChoice='', parent=None):
        super().__init__(parent)
        self.ticChoice = ticChoice
        self.tocChoice = tocChoice
        label = QtWidgets.QLabel(Consts.COMBOBOX[0])
        self.wBpm = QtWidgets.QLineEdit(str(bpm))
        self.wBar = QtWidgets.QLineEdit(str(bar))
        iconSound = QtGui.QIcon('icons/speaker.png')
        buttonSounds = QtWidgets.QPushButton()
        buttonSounds.setIcon(iconSound)
        buttonSounds.clicked.connect(self.onSounds)
        iconDelete = QtGui.QIcon('icons/trash.png')
        buttonDelete = QtWidgets.QPushButton()
        buttonDelete.setIcon(iconDelete)
        buttonDelete.clicked.connect(self.onDelete)

        self.addWidget(label)
        self.addWidget(self.wBpm)
        self.addWidget(self.wBar)
        # self.addStretch() # This breaks onDelete(). It seems it doesn't count as an item
        self.addWidget(buttonSounds)
        self.addWidget(buttonDelete)

    def onDelete(self):
        for i in range(self.count()):
            w = self.itemAt(i)
            w.widget().deleteLater()
        self.deleteLater()

    def onSounds(self):
        settingsMini = SettingsMini(ticChoice=self.ticChoice, tocChoice=self.tocChoice)
        settingsMini.exec_()
        if settingsMini.container == None:
            print('SettingsMini canceled')
            return
        self.ticChoice = settingsMini.container.ticChoice
        self.tocChoice = settingsMini.container.tocChoice

class Polyrhythms(QtWidgets.QDialog):
    def __init__(self, container=None, parent=None):
        super().__init__(parent)
        self.setModal(True)
        self.resize(300, 120)

        if container == None:
            self.container = ContainerPolyrhythms().load()
        else:
            self.container = container

        self.setWindowTitle(Consts.COMBOBOX[2])
        layout = QtWidgets.QVBoxLayout()
        
        layoutHeader = QtWidgets.QHBoxLayout()
        labelHeader = QtWidgets.QLabel(Consts.COMBOBOX[2])
        buttonAdd = QtWidgets.QPushButton('Add Rhythm')
        buttonAdd.clicked.connect(self.onAdd)
        layoutHeader.addWidget(labelHeader)
        layoutHeader.addStretch()
        layoutHeader.addWidget(buttonAdd)

        self.layoutRhythms = QtWidgets.QVBoxLayout()

        layoutButtons = QtWidgets.QGridLayout()
        buttonOk = QtWidgets.QPushButton('Ok')
        buttonOk.clicked.connect(self.onOk)
        buttonCancel = QtWidgets.QPushButton('Cancel')
        buttonCancel.clicked.connect(self.onCancel)
        buttonSave = QtWidgets.QPushButton('Save')
        buttonSave.clicked.connect(self.onSave)
        buttonLoad = QtWidgets.QPushButton('Load')
        buttonLoad.clicked.connect(self.onLoad)
        layoutButtons.addWidget(buttonOk, 0, 0)
        layoutButtons.addWidget(buttonCancel, 0, 1)
        layoutButtons.addWidget(buttonSave, 1, 0)
        layoutButtons.addWidget(buttonLoad, 1, 1)

        layout.addLayout(layoutHeader)
        layout.addLayout(self.layoutRhythms)
        layout.addLayout(layoutButtons)
        self.setLayout(layout)

        self.containerToUi()

    def containerToUi(self):
        # No clear() method
        for r in self.layoutRhythms.children():
            r.onDelete()
        for r in self.container.rhythms:
            self.layoutRhythms.addLayout(LayoutRhythm(r.bpm, r.bar, r.ticChoice, r.tocChoice))

    def uiToContainer(self):
        rhythms = []
        for wr in self.layoutRhythms.children():
            try:
                bpm = float(wr.wBpm.text())
                bar = float(wr.wBar.text())
            except:
                print('bpm and bar must be float')
                return False
            if bpm <= 0 or bar < 0:
                print('bpm > 0 and bar >= 0')
                return False
            rhythms.append(Rhythm(bpm, bar, wr.ticChoice, wr.tocChoice))

        self.container.rhythms = rhythms

        return True

    def onAdd(self):
        self.layoutRhythms.addLayout(LayoutRhythm())

    def onCancel(self):
        self.close()

    def onOk(self):
        if not self.uiToContainer():
            return # Retry
        self.close()

    def onSave(self):
        if not self.uiToContainer():
            return
        self.container.write()

    def onLoad(self):
        self.container = ContainerPolyrhythms().load()
        self.containerToUi()

